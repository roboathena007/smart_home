package com.example.smarthome

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.example.mqtt.MqttStatusListener
import com.example.mqtt.MqttManagerImpl
import kotlinx.android.synthetic.main.activity_main.*
import org.eclipse.paho.client.mqttv3.MqttMessage

const val TAG = "msg"
const val serverUri = "tcp://192.168.1.138:1883"
const val subscriptionTopic = "test/#"

open class MainActivity : AppCompatActivity() {

    private lateinit var mContext: Context
    private var clientId = "MyAndroidClientId" + System.currentTimeMillis()
    private lateinit var mqttManager: MqttManagerImpl

    fun getContext(): Context {
        return mContext
    }


    fun setContext(mContext: Context?) {
        this.mContext = mContext!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mqttManager = MqttManagerImpl(
            applicationContext,
            serverUri,
            clientId,
            arrayOf(subscriptionTopic),
            IntArray(1) { 0 })
        mqttManager.init()
        initMqttStatusListener()
        mqttManager.connect()
    }

    private fun initMqttStatusListener() {
        mqttManager.mqttStatusListener = object : MqttStatusListener {
            override fun onConnectComplete(reconnect: Boolean, serverURI: String) {
                if (reconnect) {
                    Log.d(TAG, "Reconnected to : $serverURI")
                } else {
                    Log.d(TAG, "Connected to : $serverURI")
                }
            }


            override fun onConnectFailure(exception: Throwable) {
                Log.d(TAG, "Failed to connect")
            }

            override fun onConnectionLost(exception: Throwable) {
                Log.d(TAG, "The Connection was lost")
            }

            override fun onTopicSubscriptionSuccess() {
                Log.d(TAG, "Subscribed!")
            }

            override fun onTopicSubscriptionError(exception: Throwable) {
                Log.d(TAG, "Failed to subscribe")
            }

            override fun onMessageArrived(topic: String, message: MqttMessage) {
                displayInMessagesList(String(message.payload))
            }
        }
    }
    private fun displayInMessagesList(message: String) {
        textLogs.apply {
            text = message
        }
    }
}