package com.example.manager

import android.content.Context

    interface MqttManager {
        fun init()
        fun connect()
        fun sendMessage(message: String, topic: String)
    }
